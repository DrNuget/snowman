extends KinematicBody2D

func _ready():
	pass
func _process(delta):
	get_node("Sprite/Animation").play("default")
	move_and_slide(Vector2(0, 500)*delta, Vector2(0, -1))


func _on_PlayerDetector_body_entered(body):
	if body==get_node("../Player"):
		get_node("../Player").points+=1
		get_parent().remove_child(self)
