extends KinematicBody2D

const GRAVITY: = 500
const UP = Vector2(0, -1)
var direction: = 1
var motion: = Vector2()
export var speed: = 5000

func _ready():
	pass

func _process(_delta):
	get_node("Sprite").flip_h=false if direction==1 else true
	if direction!=0:
		get_node("Sprite/Animation").play("walking")
	else:
		get_node("Sprite/Animation").stop()

func _physics_process(delta):
	if is_on_wall():
		direction=-direction
	motion.x=direction*speed
	motion.y+=GRAVITY
	move_and_slide(motion*delta, UP)


func _on_PlayerDetector_body_entered(body):
	if body==get_node("/root/map/Player"):
		body.hp-=1


func _on_StompDetector_body_entered(body):
	if body==get_node("/root/map/Player"):
		get_parent().remove_child(self)
		body.points+=1
