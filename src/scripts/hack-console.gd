extends LineEdit

func echo(txt):
	$"Output".text+=txt+"\n"
func printf(txt):
	$"Output".text+=txt

func process_command(cmd):
	cmd=cmd.split(" ")
	if cmd[0]=="echo":
		cmd.remove(0)
		if cmd.size()>0:
			echo(PoolStringArray(cmd).join(" "))
		else:
			echo("Usage: echo (text)")
	elif cmd[0]=="clear":
		$"Output".text=""
	elif cmd[0]=="hp":
		if cmd.size()>1:
			$"/root/map/Player".hp=int(cmd[1])
		else:
			echo("Usage: hp (int)")
	elif cmd[0]=="tp":
		if cmd.size()>2:
			$"/root/map/Player".global_position=Vector2(int(cmd[1]), int(cmd[2]))
		else:
			echo("Usage: tp (int) (int)")
	elif cmd[0]=="game-info":
		$"/root/map/Player/Camera/Player-Info".disp_info=!$"/root/map/Player/Camera/Player-Info".disp_info
	elif cmd[0]=="summon":
		$"/root/map".add_child($"/root/map/"+cmd[1])

func _ready():
	visible=false

func _process(_delta):
	if Input.is_action_just_pressed("haxor-console"):
		visible=!visible
		$"/root/map/Player".inp_process=!visible
	if !visible:
		pass
	elif Input.is_action_just_pressed("ui_enter"):
		process_command(text)
		text=""
