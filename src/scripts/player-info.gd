extends RichTextLabel

var hp: = 0
var disp_info: = false

func _ready():
	pass

func _process(_delta):
	bbcode_text="HP "+str(hp)
	bbcode_text+="\nPoints "+str($"../../../Player".points)
	if disp_info:
		bbcode_text+="\nFPS "+str(Engine.get_frames_per_second())+"\nPOS "+str($"../../../Player".global_position)
