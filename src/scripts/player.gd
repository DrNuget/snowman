extends KinematicBody2D
class_name Player

var direction: = Vector2()
var motion: = Vector2()

const GRAVITY = 500
const UP = Vector2(0, -1)

export var speed: = 15000
export var jump_strength: = -23
export var hp: = 5
export var points: = 0
var inp_process: = true
var djump=true

func input_process():
	direction.x=Input.get_action_strength("move_right")-Input.get_action_strength("move_left")
	if is_on_floor():
		djump=true
	if is_on_floor() && Input.is_action_just_pressed("jump"):
		motion.y=jump_strength
	elif !is_on_floor() && Input.is_action_just_pressed("jump") && djump:
		djump=false
		motion.y=jump_strength

func _ready():
	jump_strength=jump_strength*GRAVITY	

func _process(_delta):
	hp=-1 if hp<-1 else hp
	get_node("Camera/Player-Info").hp=hp
	if hp==0:
		$"Camera/Kill".visible=true
	if direction.x>0:
		$"Sprite".flip_h=false
	elif direction.x<0:
		$"Sprite".flip_h=true
	if direction.x!=0 && is_on_floor():
		get_node("Sprite/Animation").play("walking")
	elif is_on_floor():
		get_node("Sprite").frame=0
		get_node("Sprite/Animation").stop()
	else:
		get_node("Sprite").frame=3
		get_node("Sprite/Animation").stop()
	if global_position.y>500:
		hp-=1

func _physics_process(delta):
	motion.x=direction.x*speed
	if !is_on_floor():
		motion.y+=GRAVITY
	move_and_slide(motion*delta, UP)
	if inp_process:
		input_process()
